Yukaimaps Website
=================

Yukaimaps website React frontend.


Develop
-------

Requires:

- `node` >= 18
- `yarn` v1


Install dependencies:

```
yarn install
```

Run locally:

```
yarn start
```

And visit <http://127.0.0.1:1234>.
