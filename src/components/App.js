import React from 'react';

import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";

import { OIDC_CONFIG, HIDE_CONFIG } from '@src/config';
import Root from './Root';
import Home from './pages/Home';
import WorkingZones from './pages/WorkingZones';
import Edit from './pages/Edit';
import Export from './pages/Export';
import Account from './pages/Account';
import Configure from './pages/Configure';
import ConfigWizard from './pages/ConfigWizard';
import ConfigHome from './pages/ConfigHome';

import MobileApp from './pages/MobileApp';

const HAS_CONFIG_ROUTE = !HIDE_CONFIG;

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    children: [
      {
        path: "",
        element: <Home />,
      },
      {
        path: "account",
        element: <Account />,
      },
      {
        path: "configure",
        element: <Configure />,
        children: HAS_CONFIG_ROUTE  ? [
          {
            index: true,
            element: <ConfigHome />,
          },
          {
            path: "wizard",
            element: <ConfigWizard />,
          }
        ] : [],
      },
      {
        path: "working-zones",
        element: <WorkingZones />,
      },
      {
        path: "edit",
        element: <Edit />,
      },
      {
        path: "export",
        element: <Export />,
      },
      {
        path: "app",
        element: <MobileApp />,
      }
    ],
  },
]);

const onSignIn = () => {
  redirect('/');
}

export function App() {
  return (
      <RouterProvider router={router}/>
  );
}
