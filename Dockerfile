FROM node:18

USER node

RUN mkdir -p /home/node/app

WORKDIR /home/node/app

COPY --chown=node:node package.json ./
COPY --chown=node:node yarn.lock ./
RUN yarn install
COPY --chown=node:node . ./

RUN mkdir -p dist
RUN mkdir -p .parcel-cache

CMD ["yarn", "build"]

